<?php

class flickrHandler{

    private $service_search_url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search';
	private $service_filesize_url = 'https://api.flickr.com/services/rest/?method=flickr.photos.getSizes';
	private $api_key = '57f694132e4714c29a64c9af890b124e';

    public function __construct($search = NULL) {
		$perPage = 15;
		$this->service_search_url.= '&api_key='.$this->api_key;
		$this->service_search_url.= '&tags='.$search;
		$this->service_search_url.= '&per_page='.$perPage;
		$this->service_search_url.= '&format=json';
		$this->service_search_url.= '&nojsoncallback=1';
		
		$this->service_filesize_url.= '&api_key='.$this->api_key;
		$this->service_filesize_url.= '&format=json';
		$this->service_filesize_url.= '&nojsoncallback=1';
    }

    private function getSearchResult(){
		$response = json_decode(file_get_contents($this->service_search_url));
		return $response;
    }
	
	public function getImagesData(){
		$pureResult = $this->getSearchResult();
		$photos = array();
		foreach($pureResult->photos->photo as $photo){
			$get_size_url = $this->service_filesize_url;
			$get_size_url .= '&photo_id='.$photo->id;
			$response = json_decode(file_get_contents($get_size_url));
			$a_photo = array();
			if($response){
				foreach($response->sizes->size as $allsizes){
					if($allsizes->label == 'Medium'){
						$a_photo['url'] = $allsizes->source;
						$a_photo['size'] = $allsizes->width.'x'.$allsizes->height;
						break;
					}
				}
			}
			$a_photo['title'] = $photo->title;
			$photos[] = $a_photo;
		}
		return $photos;
    }
}
?>