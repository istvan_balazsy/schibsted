<?php
$searchword = '';
if($_REQUEST){
	require_once('class/flickrHandler.class.php');
	$flickr = new flickrHandler($_REQUEST['typeahead']);
	$data = $flickr->getImagesData();
	$searchword = $_REQUEST['typeahead'];
}
?>
<html>
<head>
	<title>Schibsted test</title>
	<meta charset="UTF-8">
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="typeahead.min.js"></script>
	<script>
    $(document).ready(function(){
		$('input.typeahead').typeahead({
			name: 'typeahead',
			remote:'ajax.php?key=%QUERY',
			limit : 10
		});
	});
	
	$(document).on('click', '.tt-suggestion p', function (){
		var e = $.Event('focus');
		$('#searchbox').trigger(e);
	});
	
    </script>
    <style type="text/css">
.bs-example{
	position: relative;
	margin: 50px;
}
.typeahead, .tt-hint {
	border: 2px solid #CCCCCC;
	font-size: 14px;
	height: 30px;
	line-height: 30px;
	outline: medium none;
	padding: 8px 12px;
	width: 396px;
}
.typeahead {
	background-color: #FFFFFF;
}
.tt-dropdown-menu {
	background-color: #FFFFFF;
	border: 1px solid rgba(0, 0, 0, 0.2);
	box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
	padding: 0px 0;
	width: 422px;
}
.tt-suggestion {
	font-size: 14px;
	line-height: 24px;
	padding: 3px 12px;
}
.tt-suggestion.tt-is-under-cursor {
	background-color: #0097CF;
	color: #FFFFFF;
}
.tt-suggestion p {
	margin: 0;
}
</style>
	</head>
	<body>
		<div>
			<div class="bs-example">
				<form method="post">
					<input type="text" id="searchbox" name="typeahead" class="typeahead" autocomplete="on" spellcheck="false" placeholder="keresés" value="<?php echo $searchword?>">
					<input type="submit" style="display:none"/>
				</form>
			</div>
		</div>
		<?php
		if(isset($data)){
			foreach($data as $d){?>
		<div style="text-align:center;">
			<div style="text-align:center;"><img src="<?php echo $d['url']?>"/></div>
			<div style="text-align:center;padding-bottom:12px;font-family:Verdana;font-size:10px;">
				<?php echo $d['title']?> <i>(<?php echo $d['size']?>)</i>
			</div>
		</div>
		<?php
			}
		}
		?>
	</body>
</html>